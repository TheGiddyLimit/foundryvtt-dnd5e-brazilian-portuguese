Contributing
============

If you would like to contribute for the FoundryVTT 5th Edition System localization into the Portuguese language you can do so in one of the following ways:

1. Translating the strings to the Portuguese language in the [project](https://www.transifex.com/foundryvtt-brasil/dnd-5th-edition/dashboard/) created in Transifex.

2. Sending merge requests to this repository with the [pt-BR.json](https://gitlab.com/Caua539/foundryvtt-dnd5e-brazilian-portuguese/blob/master/dnd5e_pt-BR/pt-BR.json) file with its translated strings.

3. Creating new [issues](https://gitlab.com/Caua539/foundryvtt-dnd5e-brazilian-portuguese/issues) in this repository containing any new translation or error that you found.