[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Ffoundryvtt-dnd5e-brazilian-portuguese%2F-%2Fraw%2Fmaster%2Fdnd5e_pt-BR%2Fmodule.json)](https://foundryvtt.com/packages/translation_dnd5e_ptBR) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Ffoundryvtt-dnd5e-brazilian-portuguese%2F-%2Fraw%2Fmaster%2Fdnd5e_pt-BR%2Fmodule.json)](http://foundryvtt.com/) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Ffoundryvtt-dnd5e-brazilian-portuguese%2F-%2Fraw%2Fmaster%2Fdnd5e_pt-BR%2Fmodule.json)](http://foundryvtt.com/) [![DnD 5e Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=5e%20Compatible&prefix=v&query=minimumSystemVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Ffoundryvtt-dnd5e-brazilian-portuguese%2F-%2Fraw%2Fmaster%2Fdnd5e_pt-BR%2Fmodule.json)](https://foundryvtt.com/packages/dnd5e/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discord.gg/Ay42VG)

FoundryVTT 5th Edition Brazilian Portuguese
=================================

## Português

Esse módulo adiciona o idioma *Português (Brasil)* como uma opção a ser selecionada nas configurações do FoundryVTT. Selecionar essa opção traduzirá vários aspectos da aplicação relacionados ao sistema [D&D 5E](https://gitlab.com/foundrynet/dnd5e "Foundry VTT 5th Edition"), como as fichas de personagem, rolagens, compêndio de magias (com o módulo [Babele](https://foundryvtt.com/packages/babele/ "Babele")), entre outros.
  
Esse módulo traduz somente aspectos relacionados ao sistema de D&D 5E. Esse módulo não traduz outras partes do software FoundryVTT, como a interface principal. Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Se encontrar algum erro na tradução ou tiver uma sugestão de algum termo melhor para ser usado em alguma parte dela você pode abrir uma nova [*issue*](https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/issues "issues") ou enviar uma mensagem para **Melithian539#0625** no Discord.

> Atualizado para funcionar com a versão 0.7.9 do FoundryVTT e com a versão 1.24 do sistema 5th Edition.

### Instalação

A tradução está disponível na lista de Módulos Complementares para instalar com o nome de `Brazilian Portuguese for [D&D 5e]`.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/raw/master/dnd5e_pt-BR/module.json`

#### Instalação Manual

Se as opções acima não funcionarem, faça o download do arquivo [dnd5e_pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/jobs/artifacts/master/raw/dnd5e_pt-BR.zip?job=build "dnd5e_pt-BR.zip") e extraia o conteúdo dele dentro da pasta `Data/modules/`

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.


---


## English

This module adds the language *Português (Brasil)* as an option to be selected in the FoundryVTT settings. Selecting this option will translate various aspects of the application's UI related to the [DnD 5E](https://gitlab.com/foundrynet/dnd5e "Foundry VTT 5th Edition") system, like character sheets, rolls, spell compendiums (with the [Babele](https://foundryvtt.com/packages/babele/ "Babele") module), among others.

This module translates only aspects related to the D&D 5E system. This module doesn't translate other parts of the FoundryVTT software, like the main interface. For that, take a look at the [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/) module.

If you find a mistake in the translation or have a suggestion of a better term to use somewhere in it you can open a new [*issue*](https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/issues "issues") or send a message to **Melithian539#0625** at Discord.

> Updated to work with version 0.7.9 of FoundryVTT and with version 1.24 of the 5th Edition System.

### Installation

The translation is available in the Add-on Modules list to install with the name `Brazilian Portuguese for [D&D 5e]`.

#### Manifest Installation

In the `Add-On Modules` option click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/raw/master/dnd5e_pt-BR/module.json`

#### Manual Installation

If the above options do not work, download the [dnd5e_pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/foundryvtt-dnd5e-brazilian-portuguese/-/jobs/artifacts/master/raw/dnd5e_pt-BR.zip?job=build "dnd5e_pt-BR.zip") file and extract its contents into the `Data/modules/` folder

Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.